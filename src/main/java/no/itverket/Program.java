package no.itverket;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        final Deck deck = new Deck();
        final List<Card> playerHand = new ArrayList<>();
        final List<Card> dealerHand = new ArrayList<>();

        int playerTotal = 0;
        int dealerTotal = 0;


        final Scanner scanner = new Scanner(System.in);
        Card dealerCard = getCard(deck, dealerHand);
        dealerTotal = calculatePoints(dealerHand);
        System.out.printf("Dealer's first card is %s of %s.%n", dealerCard.rank, dealerCard.suit);
        System.out.println(dealerTotal);

        while (true) {
            System.out.println("Stand, Hit");
            final String read = scanner.nextLine().toLowerCase();

            if (read.equals("hit")) {
                Card playerCard = getCard(deck, playerHand);
                playerTotal = calculatePoints(playerHand);

                //oppgave 1
                if(playerTotal > 21) {
                    System.out.printf("Hit with %s of %s.%n", playerCard.rank, playerCard.suit);
                    System.out.printf("You have: %s points%n", playerTotal);
                    System.out.println("You had over 21 points, You lost.");
                    break;
                } else {
                    System.out.printf("Hit with %s of %s. Total is %s points.%n", playerCard.rank, playerCard.suit, playerTotal);
                }

            } else if (read.equals("stand")) {
                //oppgave 5
                while(dealerTotal <= 17) {
                    Card card = getCard(deck, dealerHand);
                    dealerTotal = calculatePoints(dealerHand);
                    System.out.printf("Dealer's got the %s of %s card.%n" , card.rank, card.suit);
                }
                winCheck(dealerTotal,playerTotal);
                break;
            }
        }
    }

    //oppgave 6
    public static void winCheck(int dealerTotal, int playerTotal) {
        if(playerTotal == 21) {
            System.out.printf("You have: %s points%n", playerTotal);
            System.out.println("You got Blackjack! You win!");
        } else if (dealerTotal == 21) {
            System.out.printf("Dealer has: %s points%n", dealerTotal);
            System.out.println("Dealer got Blackjack! You lost.");
        } else if (playerTotal > 21) {
            System.out.printf("You have: %s points%n", playerTotal);
            System.out.println("You had over 21 points, You lost.");
        } else if (dealerTotal > 21) {
            System.out.printf("Dealer has: %s points%n", dealerTotal);
            System.out.println("Dealer had over 21 points, Dealer lost.");
        } else if (playerTotal == dealerTotal) {
            System.out.printf("You have: %s points%n", playerTotal);
            System.out.printf("Dealer has: %s points%n", dealerTotal);
            System.out.println("It's a tie!");
        } else if (playerTotal < dealerTotal) {
            System.out.printf("You have: %s points%n", playerTotal);
            System.out.printf("Dealer has: %s points%n", dealerTotal);
            System.out.println("Dealer points are higher. You lost.");
        } else {
            System.out.printf("You have: %s points%n", playerTotal);
            System.out.printf("Dealer has: %s points%n", dealerTotal);
            System.out.println("Your points are higher! You won!.");
        }
    }

    public static Card getCard(Deck deck, List<Card> hand) {
        final Card card = deck.cards.remove();
        hand.add(card);
        return card;
    }

    public static int calculatePoints(List<Card> hand) {
        int total = hand.stream().map(x -> Math.min(x.rank.getValue(), 10)).reduce(0, Integer::sum);
        int acePoint = 10;

        //oppgave 3
        if(hand.stream().anyMatch(x -> x.rank.equals(Rank.A))) {
            if (total <= 11) {
                return total + acePoint;
            }
        }
        return total;
    }
}
