package no.itverket;

import java.util.*;

class Deck {
    //byttet Queue med LinkedList for å kunne bruke Collections.shuffle funksjon.
    LinkedList<Card> cards;

    Deck() {
        cards = new LinkedList<>();
        for (Suit suit : Suit.values()) {
            for (Rank rank : Rank.values()) {
                cards.offer(new Card(suit, rank));
            }
        }
        //oppgave 4
        Collections.shuffle(cards);
    }
}
